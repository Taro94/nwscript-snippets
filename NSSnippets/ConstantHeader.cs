namespace NSSnippets
{
    public class ConstantHeader
    {
        public string Type { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}